package utils;

import controllers.BookingController;
import controllers.FlightController;
import entities.Booking;
import entities.Flight;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
class NotANumberEx extends Exception  {
	public NotANumberEx(){
		super();
	}
}

public class InputHandler {
	FlightController FC;
	BookingController BC;

	Printer print;

	public InputHandler(Printer p, FlightController f, BookingController b) {
		print = p;
		FC = f;
		BC = b;
	}

	Scanner sc = new Scanner(System.in);

	int parseInt(String a) throws NotANumberEx {
		try {
			int parsedInput = Integer.parseInt(a);
			return parsedInput;
		}catch (Exception ex){
			throw new NotANumberEx();
		}
	}

	public void run() {
		while (true) {
			print.menu();
			print.input();
			String input = sc.next();
			if (Objects.equals(input.toLowerCase().trim(), "exit") || Objects.equals(input.trim(), "6")) {
				print.exit();
				break;
			} else {
				try {
					int parsedInput = parseInt(input);
					handleInput(parsedInput);
				} catch (NotANumberEx ex) {
						print.notANumberEx();
				}
			}
		}
	}

	void searchAndBook() {
		String input;
		print.output();
		System.out.println("date in yyyy-mm-dd format");
		print.output();
		System.out.println("destination");
		print.output();
		System.out.println("places to book amount");
		print.input();
		input = sc.next();
		try {
			LocalDate parsedDate = dateParse(input);
			print.input();
			String destInput = sc.next();
			outerloop:
			while (true){print.input();
				String amountInput = sc.next();
				try {
					int parsedAmountInput = parseInt(amountInput);
					print.output();
					System.out.println("your request - " + destInput.toUpperCase() + " | " + parsedDate + " | " + parsedAmountInput);
					List<Flight> req = FC.getBy(destInput, parsedDate, parsedAmountInput);
					if (req.size() == 0) {
						print.output();
						System.out.println("no matches");
						break;
					} else {
						print.output();
						System.out.println("found " + req.size() + " flight(s) |");
						print.timeTableFrom(req, true);
						print.divider();
						print.output();
						System.out.println("book ?                        [y/n]");
						while (true) {
							print.input();
							String bookInputBool = sc.next();
							if (Objects.equals(bookInputBool.toLowerCase().trim(), "y")) {
								print.output();
								System.out.println("id");
								print.input();
								String inputBookId = sc.next();
								if (req.stream().map(Flight::getId).toList().contains(inputBookId)) {

									int index = req.stream().map(Flight::getId).toList().indexOf(inputBookId);
									Flight f = req.get(index);

									print.output();
									System.out.println("client name    |");
									print.output();
									System.out.println("client surname |");

									print.input();
									String name = sc.next();
									print.input();
									String surname = sc.next();

									if (f.getCapacity() > parsedAmountInput) {
										int ID = BC.createBooking(name, surname, f, parsedAmountInput);
										print.divider();
										print.output();
										System.out.println("SUCCESS: ");
										print.output();
										System.out.println(BC.getAll().get(BC.getAll().stream().map(booking -> booking.getID()).toList().indexOf(ID)));
										break outerloop;
									} else {
										System.out.println("not enough free places");
									}

								} else {
									print.output();
									System.out.println("incorrect ID");
									print.output();
									System.out.println("do you want to book? [y/n]");
								}
							} else if (Objects.equals(bookInputBool.toLowerCase().trim(), "n")) {
								print.output();
								System.out.println("canceling");
								break outerloop;
							}else{
								print.output();
								System.out.println("incorrect answer");
							}
						}
					}
				} catch (NotANumberEx ex) {
					print.output();
					System.out.println("not a number, try again");
				}}

		} catch (Exception ex) {
			print.output();
			System.out.println("incorrect date, check spelling [-]");
		}
	}

	void giveBookings() {
		print.output();
		System.out.println("client name");
		print.output();
		System.out.println("client surname");
		print.input();
		String name = sc.next();
		print.input();
		String surname = sc.next();

		List<Booking> req = BC.getAll();
		List<Booking> sorted =
				req.stream().filter(booking -> Objects.equals(booking.getClientName().toLowerCase().trim(), name.toLowerCase().trim()) && Objects.equals(booking.getClientSurname().toLowerCase().trim(),
						surname.toLowerCase().trim()) && booking.getBooking().getDate() > System.currentTimeMillis()).toList();
		print.booking(sorted);
	}

	void deleteBooking() {
		print.output();
		System.out.println("client name");
		print.output();
		System.out.println("client surname");
		print.input();
		String name = sc.next();
		print.input();
		String surname = sc.next();

		List<Booking> req = BC.getAll();
		List<Booking> sorted =
				req.stream().filter(booking -> Objects.equals(booking.getClientName().toLowerCase().trim(), name.toLowerCase().trim()) && Objects.equals(booking.getClientSurname().toLowerCase().trim(),
						surname.toLowerCase().trim()) && booking.getBooking().getDate() > System.currentTimeMillis()).toList();
		print.output();
		System.out.println("your bookings: ");
		print.divider();
		print.booking(sorted);
		while (true) {
			print.output();
			System.out.println("ID to cancel");
			print.input();
			String ID = sc.next();
			try {
				int parsedID = parseInt(ID);
				if (sorted.stream().map(Booking::getID).toList().contains(parsedID)) {
					print.output();
					System.out.println("cancelling");
					BC.cancel(parsedID);
					break;
				} else {
					print.output();
					System.out.println("incorrect ID, try again");
				}
			} catch (NotANumberEx ex) {
				print.output();
				System.out.println("incorrect ID, try again");
			}

		}

	}

	void handleInput(int entry) {
		String input;
		switch (entry) {
			case (1):
				print.timeTable();
				break;
			case (2):
				print.input();
				input = sc.next();
				getById(input);
				break;
			case (3):
				searchAndBook();
				break;
			case (4):
				deleteBooking();
				break;
			case (5):
				giveBookings();
				break;

			default:
				print.outOfBoundsEx();
		}
	}

	LocalDate dateParse(String input) {
		return LocalDate.parse(input);
	}

	void getById(String id) {
		try {
			Flight f;
			f = FC.getById(id);
			print.output();
			System.out.println("Flight by identification number: ");
			print.output();
			System.out.println(f.toStringWithPlaces());
		} catch (FlightNotFindException ex) {
			print.output();
			System.out.println("ERROR OCCURRED!: incorrect ID");
		}
	}
}
