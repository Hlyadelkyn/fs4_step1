package utils;

import controllers.FlightController;
import entities.Booking;
import entities.Flight;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Printer {
	FlightController FC;

	public Printer(FlightController a) {
		FC = a;
	}

	public Printer() {}

	public void indent() {
		System.out.print("      ");
	}


	public void divider() {
		indent();
		System.out.println("________________________________");
	}

	public void greetings() {
		divider();
		indent();
		System.out.println(" WELCOME TO BOOKINGMANAGER v0.1 ");
	}

	public void menu() {
		divider();
		indent();
		System.out.println(" |  1 << View Timetable                            (no input required)  |");
		indent();
		System.out.println(" |  2 << View Flight                               (ID input required)  |");
		indent();
		System.out.println(" |  3 << Search and Book Flight  (multiple information input required)  |");
		indent();
		System.out.println(" |  4 << Cancel Booking          (multiple information input required)  |");
		indent();
		System.out.println(" |  5 << My Bookings             (multiple information input required)  |");
		indent();
		System.out.println(" |  6 << Exit                                      (no input required)  |");
	}
	public void input(){
		System.out.print(">> ");
	}
	public void output(){
		System.out.print("<< ");
	}

	public void timeTable() {
		int index = 1;
		List<Flight> list = FC.getAll();
		Stream<Flight> str = list.stream().filter(flight -> (flight.getDate() < (System.currentTimeMillis() + 86400000L)) && (flight.getDate() > System.currentTimeMillis()));
		List<Flight> sortedList = str.collect(Collectors.toList());
		for (Flight f : sortedList
		) {
			if (index < 10) {
				System.out.println(" | " + index++ + "   | " + f);
			} else if (index < 100) {
				System.out.println(" | " + index++ + "  | " + f);
			} else {
				System.out.println(" | " + index++ + " | " + f);
			}
		}
	}

	public void timeTableFrom(List<Flight> i, boolean withSeats) {
		int index = 1;
		if (withSeats == true) {
			for (Flight f : i
			) {
				if (index < 10) {
					System.out.println(" | " + index++ + "   | " + f.toStringWithPlaces());
				} else if (index < 100) {
					System.out.println(" | " + index++ + "  | " + f.toStringWithPlaces());
				} else {
					System.out.println(" | " + index++ + " | " + f.toStringWithPlaces());
				}
			}
		} else {
			for (Flight f : i
			) {
				if (index < 10) {
					System.out.println(" | " + index++ + "   | " + f);
				} else if (index < 100) {
					System.out.println(" | " + index++ + "  | " + f);
				} else {
					System.out.println(" | " + index++ + " | " + f);
				}
			}
		}

	}

	public void booking(List<Booking> b1){
		if (b1.size() == 0) {
			output();
			System.out.println("NO ACTIVE BOOKINGS FOUND");
		} else {
			int index = 1;
			for (Booking b : b1
			) {
				if (index < 10) {
					System.out.println(" | " + index++ + "   | " + b.toString());
				} else if (index < 100) {
					System.out.println(" | " + index++ + "  | " + b.toString());
				} else {
					System.out.println(" | " + index++ + " | " + b.toString());
				}
			}
		}

	}
	public void exit() {
		divider();
		indent();
		System.out.println("           ALL BEST!            ");
	}

	public void notANumberEx() {
		output();
		System.out.println("ERROR OCCURRED! (check input for being integer number)");
	}

	public void outOfBoundsEx() {
		output();
		System.out.println("ERROR OCCURRED! (out of bounds)");
	}

	public void notADateEx() {
		output();
		System.out.println("ERROR OCCURRED! (check date spelling)");
	}

}
