package utils;

import entities.Airline;
import entities.Airlines;
import entities.Cities;
import entities.Flight;

import java.util.ArrayList;
import java.util.List;

public class FlightGenerator {
	List<Integer> IDLIST = new ArrayList<>();

	public Flight getRandomFlight() {
		Flight tempflight = new Flight();
		Airline randAr = getRandomAirline();
		tempflight.setAirline(randAr);
		tempflight.setDestination(getRandomCity());
		tempflight.setDate(getRandomDateMillis());
		tempflight.setId(getID(randAr));
		tempflight.setCapacity(getRandomCapacity());
		return tempflight;
	}

	public List<Flight> run() {
		List<Flight> FL = new ArrayList<>();
		for (int i = 0; i < 260; i++) {
			Flight tempflight = getRandomFlight();
			FL.add(tempflight);
		}
		return FL;
	}

	long getRandomDateMillis() {
		long date = (long) (Math.random() * (345600000L) + System.currentTimeMillis());
		return date;
	}
	String getID(Airline inputAR){
		Integer nums = (int) (Math.random() * 999);
		while(true){
			if(IDLIST.contains(nums)){
				nums= (int) (Math.random() * 999);
			}else{
				IDLIST.add(nums);
				return inputAR.getAbbreviation()+nums;
			}
		}
	}
	int getRandomCapacity(){
		return (int)(Math.random()*30);
	}

	String getRandomCity() {
		Cities cities = new Cities();
		cities.init();
		int size = cities.getDb().size();

		int b = (int) (Math.random() * (size));
		return cities.getDb().get(b);
	}
	Airline getRandomAirline(){
		Airlines airlines = new Airlines();
		airlines.init();
		int size = airlines.getDb().size();

		int b = (int)(Math.random()*size);
		return airlines.getDb().get(b);
	}
}
