package utils;

public class FlightNotFindException extends Exception  {
	public FlightNotFindException(String s){
		super(s);
	}
}
