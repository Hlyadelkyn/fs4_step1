package utils;
import controllers.BookingController;
import controllers.FlightController;
import dao.BookingDAO;
import dao.FlightDAO;
import entities.Booking;
import entities.Flight;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import service.BookingService;
import service.FlightService;

import java.util.ArrayList;
import java.util.List;

public class Tests {
	FlightDAO FD = new FlightDAO();
	BookingDAO BD = new BookingDAO();
	FlightGenerator FG = new FlightGenerator();
	FlightService FS = new FlightService(FD, FG);
	BookingService BS = new BookingService(BD, FD);
	FlightController FC = new FlightController(FS);
	BookingController BC = new BookingController(BS);

	@Test public void Test_BDAO(){
		List<Booking> l = new ArrayList<>();
		BD.setDB(l);
		assertEquals(BD.getAll().size(), 0);
		BD.loadID();
		assertEquals(BD.getCurrentID(), 0);
		BD.addIDiff();
		assertEquals(BD.getCurrentID(), 1);
	}
	@Test public void TestF_DAO(){
		Flight f = FG.getRandomFlight();
		Flight f2 = FG.getRandomFlight();
		List<Flight> l = new ArrayList<>();
		l.add(f);
		FD.setDB(l);

		assertEquals(1, FD.getAll().size());
		assertEquals(f.getId(), FD.get(FD.getAll().size()-1).getId());
		FD.create(f2);
		assertEquals(2, FD.getAll().size());
		assertEquals(f2.getId(), FD.get(FD.getAll().size()-1).getId());
		FD.delete(1);
		assertEquals(1, FD.getAll().size());
		assertEquals(f.getId(), FD.get(FD.getAll().size()-1).getId());
	}

	@Test public void Test_BS(){
		//одразу перевірив методи init save getAll createBooking cancel
		BS.init();
		//_____________________________________________
		int amount_pre =BS.getAll().size();
		Flight f = FG.getRandomFlight();
		f.setCapacity(5);
		int BID = BS.createBooking("tyler_test", "durden_test", f, 2);
		int amount_post = BS.getAll().size();
		assertEquals(amount_post,amount_pre+1);
		BS.save();
		BS.shutdownDatabase();
		BS.init();
		BS.cancel(BID);
		amount_post = BS.getAll().size();
		assertEquals(amount_post, amount_pre);
		//_____________________________________________
		BS.save();
	}



	@Test public void Test_FS() throws FlightNotFindException {
		//одразу перевірив методи init save getAll addOne
		FS.init();
		//_____________________________________________
		int amount = FS.getAll().size();
		Flight f = FG.getRandomFlight();
		FD.addOne(f);
		assertEquals(amount +1,FS.getAll().size());
		assertEquals(f, FS.getById(f.getId()));
		FS.save();
		FS.drop();
		FS.init();
		assertEquals(f.getId(), FS.getById(f.getId()).getId()); // порівнювати по айді
		//_____________________________________________
//		FS.save();
	}
	//чесно не бачу сенсу тестувати контроллєр оскільки він нічого нового не реалізує а просто виділяє в зручний "інтерфейс" методи з сервісу, ддя використання в хендлері
}
