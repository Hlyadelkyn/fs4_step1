package entities;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

public class Booking implements Serializable {
	private Flight booking;
	private int amount;
	private int ID;

	private String clientName;
	private String clientSurname;
	public Booking(String n, String s, Flight f, int a, int i) {
		clientName = n;
		clientSurname = s;
		booking = f;
		amount = a;
		ID = i;
		f.setCapacity(f.getCapacity()-a);
	}

	@Override
	public String toString() {
		return String.format("ID - %s | %s %s |  ---  flight booked  ---  | %s | PLACES ORDERED  -  %s",ID, clientName.toUpperCase(), clientSurname.toUpperCase(),
				booking.toString(),
				amount);
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public int getAmount() {
		return amount;
	}

	public int getID() {
		return ID;
	}

	public Flight getBooking() {
		return booking;
	}

	public void setBooking(Flight bookings) {
		this.booking = bookings;
	}

	public String getClientSurname() {
		return clientSurname;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setClientSurname(String clientSurname) {
		this.clientSurname = clientSurname;
	}
}
