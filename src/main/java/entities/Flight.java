package entities;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class Flight implements Serializable {
	public Flight(){}
	private String id;
	private long date;
	private String destination;
	private Airline airline;
	private int capacity;
	@Override
	public String toString (){
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date tempDate = new Date(this.date);
		int diff = 11	-destination.length();
		if(id.length() == 3){
			return String.format("%s   | %s | KYIV       --->  %s %s | %s", id, dateFormatter.format(tempDate), StringUtils.repeat(' ', diff), destination.toUpperCase() ,
					airline.getName().toUpperCase());
		}else if(id.length() == 4){
			return String.format("%s  | %s | KYIV       --->  %s %s | %s", id, dateFormatter.format(tempDate),StringUtils.repeat(' ', diff), destination.toUpperCase() ,
					airline.getName().toUpperCase());
		}else{
			return String.format("%s | %s | KYIV       --->  %s %s | %s", id, dateFormatter.format(tempDate), StringUtils.repeat(' ', diff),destination.toUpperCase() ,
					airline.getName().toUpperCase());
		}
	}
	public String toStringWithPlaces(){
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date tempDate = new Date(this.date);
		int diff = 11	-destination.length();
		int diff2 = 22 - airline.getName().length();
		if(id.length() == 3){
			return String.format("%s   | %s | KYIV --> %s %s | %s %s | %s places are free", id, dateFormatter.format(tempDate), StringUtils.repeat(' ', diff),
					destination.toUpperCase() ,
					airline.getName().toUpperCase(), StringUtils.repeat(' ', diff2), capacity);
		}else if(id.length() == 4){
			return String.format("%s  | %s | KYIV --> %s %s | %s %s | %s places are free", id, dateFormatter.format(tempDate),StringUtils.repeat(' ', diff), destination.toUpperCase() ,
					airline.getName().toUpperCase(), StringUtils.repeat(' ', diff2), capacity);
		}else{
			return String.format("%s | %s | KYIV --> %s %s | %s %s | %s places are free", id, dateFormatter.format(tempDate), StringUtils.repeat(' ', diff),destination.toUpperCase() ,
					airline.getName().toUpperCase(), StringUtils.repeat(' ', diff2), capacity);
		}
	}

	public Airline getAirline() {
		return airline;
	}

	public LocalDate getLocalDate(){
		Date d = new Date(this.date);
		LocalDate ld = LocalDate.of(d.getYear()+1900, d.getMonth()+1, d.getDate());
		return ld;
	}

	public long getDate() {
		return date;
	}

	public int getCapacity() {
		return capacity;
	}

	public String getDestination() {
		return destination;
	}

	public String getId() {
		return id;
	}

	public void setAirline(Airline airline) {
		this.airline = airline;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setId(String id) {
		this.id = id;
	}
}
