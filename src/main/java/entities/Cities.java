package entities;

import java.io.Serializable;
import java.util.*;

public class Cities implements Serializable {
	List<String> db = new ArrayList<String>();

	public void init() {
		db.add("new-york");
		db.add("Tirane");
		db.add("Berlin");
		db.add("Warsaw");
		db.add("Warna");
		db.add("Amsterdam");
		db.add("Rome");
		db.add("Bangkok");
		db.add("Tokyo");
		db.add("Paris");
		db.add("London");
		db.add("Mumbai");
		db.add("munich");
		db.add("Seoul");
		db.add("Mexico");
		db.add("Sydney");
		db.add("Chicago");
		db.add("Los Angeles");
		db.add("Taipei");
		db.add("Vienna");
		db.add("Shanghai");
		db.add("Istanbul");
		db.add("Prague");
		db.add("Madrid");
		db.add("Toronto");
		db.add("Ohio");
		db.add("Lviv");
		db.add("Singapore");
		db.add("Barcelona");
	}

	public List<String> getDb() {
		return db;
	}

	public void setDb(List<String> db) {
		this.db = db;
	}
}
