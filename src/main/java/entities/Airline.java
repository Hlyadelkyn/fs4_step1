package entities;

import java.io.Serializable;

public class Airline implements Serializable {
	private String name;
	private String abbreviation;
	Airline(String inputName, String inputAbb){
		this.name = inputName;
		this.abbreviation = inputAbb;
	}

	public String getName() {
		return name;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
}
