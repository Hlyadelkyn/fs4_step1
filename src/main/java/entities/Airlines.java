package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Airlines implements Serializable {
	List<Airline> db = new ArrayList<>();
	public void init() {
		db.add(new Airline("American Airlines", "AA"));
		db.add(new Airline("Delta Air Lines", "DL"));
		db.add(new Airline("United Airlines", "UA"));
		db.add(new Airline("Lufthansa", "LH"));
		db.add(new Airline("Air France", "AF"));
		db.add(new Airline("Emirates", "EK"));
		db.add(new Airline("British Airways", "BA"));
		db.add(new Airline("Cathay Pacific Airways", "CX"));
		db.add(new Airline("Qatar Airways", "QR"));
		db.add(new Airline("Singapore Airlines", "SQ"));
		db.add(new Airline("China Southern Airlines", "CZ"));
		db.add(new Airline("China Eastern Airlines", "MU"));
		db.add(new Airline("ANA All Nippon Airways", "NH"));
		db.add(new Airline("Turkish Airlines", "TK"));
		db.add(new Airline("Air China", "CA"));
	}

	public List<Airline> getDb() {
		return db;
	}

	public void setDb(List<Airline> db) {
		this.db = db;
	}

}
