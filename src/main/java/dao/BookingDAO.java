package dao;

import entities.Booking;

import java.util.ArrayList;
import java.util.List;

public class BookingDAO {
	List<Booking> dataBase = new ArrayList<>();
	Integer currentID;

	public List<Booking> getAll(){
		return dataBase;
	}

	public void loadID(){
		if(dataBase.size() == 0){
			currentID =0;
		}else{
			currentID = dataBase.get(dataBase.size() - 1).getID()+1;
		}
	}

	public Booking get(int inputIndex){
		return dataBase.get(inputIndex);
	}

	public void add(Booking inputObj) {
		dataBase.add(inputObj);
	}

	public void setCurrentID(Integer currentID) {
		this.currentID = currentID;
	}

	public Integer getCurrentID() {
		return currentID;
	}
	public void addIDiff(){
		this.currentID += 1;
	}

	public boolean delete(int inputIndex){
		try{
			dataBase.remove(inputIndex);
			return true;
		}catch (Exception ex){
			System.out.print(ex);
			return false;
		}
	}

	public void setDB(List<Booking> dataBase) {
		this.dataBase = dataBase;
	}
}
