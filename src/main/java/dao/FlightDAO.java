package dao;

import entities.Booking;
import entities.Flight;

import java.util.ArrayList;
import java.util.List;

public class FlightDAO {
	List<Flight> dataBase = new ArrayList<>();

	public List<Flight> getAll(){
		return dataBase;
	}

	public Flight get(int inputIndex){
		return dataBase.get(inputIndex);
	}

	public boolean create(Flight inputObj) {
		try{
			dataBase.add(inputObj);
			return true;
		}catch (Exception ex){
			return false;
		}
	}
	public void addOne(Flight f){
			dataBase.add(f);
	}

	public boolean delete(int inputIndex){
		try{
			dataBase.remove(inputIndex);
			return true;
		}catch (Exception ex){
			System.out.print(ex);
			return false;
		}
	}

	public void replaceOne(Flight f){
		dataBase.set(dataBase.stream().map(flight -> f.getId()).toList().indexOf(f.getId()), f);
	}

	public void setDB(List<Flight> dataBase) {
		this.dataBase = dataBase;
	}
}
