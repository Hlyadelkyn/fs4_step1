package service;

import dao.FlightDAO;
import entities.Flight;
import utils.FlightGenerator;
import utils.FlightNotFindException;

import java.io.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.stream;

public class FlightService {
	FlightDAO FD;
	FlightGenerator FG;

	public List<Flight> getAll() {
		return FD.getAll();
	}

	public FlightService(FlightDAO a, FlightGenerator b) {
		FD = a;
		FG = b;
	}

	public Flight getById(String id) throws FlightNotFindException {
		List<Flight> db = FD.getAll();
		Stream<String> a = db.stream().map(Flight::getId);
		int index = a.toList().indexOf(id);
		if (index != -1) {
			List<Flight> db2 = FD.getAll();
			return db2.get(index);
		} else {
			throw new FlightNotFindException("flight by " +id+ " not found");
		}
	}
	public void drop(){
		FD.setDB(null);
	}

	public List<Flight> getBy(String inputDest, LocalDate inputDate, int inputAmount) {

		Predicate<Flight> byDestDateAmount =
				flight -> flight.getCapacity() >= inputAmount && flight.getDate() > System.currentTimeMillis() && Objects.equals(flight.getDestination().toLowerCase().trim(),
						inputDest.toLowerCase().trim()) && inputDate.isEqual(flight.getLocalDate());
		List<Flight> req = FD.getAll();
		Stream<Flight> str = req.stream().filter(byDestDateAmount);
		return str.collect(Collectors.toList());
	}

	public void initTest() {
		List<Flight> list = FG.run();
		FD.setDB(list);
	}

	public void init() {
		try {
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "/src/main/java/bd/flights.txt");
			ObjectInputStream ois = new ObjectInputStream(fis);
			List<Flight> f = null;
			f = (List<Flight>) ois.readObject();
			ois.close();
			FD.setDB(f);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public void save() {
		try {
			List<Flight> l = FD.getAll();
			FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir") + "/src/main/java/bd/flights.txt");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(l);
			oos.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
