package service;

import dao.BookingDAO;
import dao.FlightDAO;
import entities.Booking;
import entities.Flight;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BookingService {
	BookingDAO BD;
	FlightDAO FD;
	public BookingService(BookingDAO a, FlightDAO f){
		FD = f;
		BD = a;
	}
	public void add(Booking inputB){
		BD.add(inputB);
	}
	public List<Booking> getAll(){
		return BD.getAll();
	}
	public int createBooking(String name, String surname, Flight f, int amount){
		Booking b = new Booking(name,surname,f,amount,BD.getCurrentID());
		int temp = BD.getCurrentID();
		BD.addIDiff();
		BD.add(b);
		return temp;
	}
	public void cancel(int ID){
		Booking b = BD.get(BD.getAll().stream().map(Booking::getID).toList().indexOf(ID));
		Flight f = b.getBooking();
		f.setCapacity(f.getCapacity()+b.getAmount());
		if(FD.getAll().contains(f)){
			FD.replaceOne(f);
		}
		BD.delete(BD.getAll().stream().map(Booking::getID).toList().indexOf(ID));
	}
	public void init() {
		try{
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "/src/main/java/bd/bookings.txt");
			ObjectInputStream ois = new ObjectInputStream(fis);
			List<Booking> b = (List<Booking>) ois.readObject();
			ois.close();
			BD.setDB(b);
		}catch (Exception ex){
			System.out.println(ex);
		}finally {
			BD.loadID();
		}

	}
	public void shutdownDatabase(){
		ArrayList<Booking> b = new ArrayList<Booking>();
		BD.setDB(b);
	}
	public void save() {
		try {
			List<Booking> l = BD.getAll();
			FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir") + "/src/main/java/bd/bookings.txt");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(l);
			oos.close();
		}catch (Exception ex){
			System.out.println(ex);
		}

	}
}
