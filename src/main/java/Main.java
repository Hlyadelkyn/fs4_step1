import utils.InputHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		BookingManager app = new BookingManager();
		app.run();
	}
}
