import controllers.BookingController;
import controllers.FlightController;
import dao.BookingDAO;
import dao.FlightDAO;
import service.BookingService;
import service.FlightService;
import utils.FlightGenerator;
import utils.InputHandler;
import utils.Printer;

import java.io.IOException;

public class BookingManager {
	FlightDAO FD = new FlightDAO();
	BookingDAO BD = new BookingDAO();
	FlightGenerator FG = new FlightGenerator();
	FlightService FS = new FlightService(FD, FG);
	BookingService BS = new BookingService(BD, FD);
	FlightController FC = new FlightController(FS);
	BookingController BC = new BookingController(BS);
	Printer print = new Printer(FC);
	InputHandler handler = new InputHandler(print, FC, BC);

	void run() {
		FC.dbInit();   // - читка з файлу
//		FC.dbFill();   // - заповнення бд

		BC.init();

		print.greetings();
		handler.run();

		FC.dbSave();
		BC.save();
	}

}
