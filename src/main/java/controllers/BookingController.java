package controllers;

import entities.Booking;
import entities.Flight;
import service.BookingService;

import java.io.IOException;
import java.util.List;

public class BookingController {
	BookingService BS;

	public BookingController(BookingService b) {
		BS = b;
	}

	public void add(Booking b) {
		BS.add(b);
	}

	public List<Booking> getAll() {
		return BS.getAll();
	}

	public int createBooking(String name, String surname, Flight f, int amount) {
		return BS.createBooking(name, surname, f, amount);
	}
	public void init () {
		BS.init();
	}
	public void save() {
		BS.save();
	}
	public void cancel(int ID){
		BS.cancel(ID);
	}

	public void shutdown(){
		BS.shutdownDatabase();
	}
}
