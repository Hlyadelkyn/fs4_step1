package controllers;

import entities.Flight;
import service.FlightService;
import utils.FlightNotFindException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class FlightController {
	FlightService FS;

	public FlightController(FlightService a) {
		FS = a;
	}
	public void dbInit() {
		FS.init();
	}
	public void dbFill(){
		FS.initTest();
	}
	public List<Flight> getAll(){
		return FS.getAll();
	}
	public Flight getById(String Id) throws FlightNotFindException {
		try {
			return FS.getById(Id);
		} catch (FlightNotFindException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	public void dbSave() {
		FS.save();
	}
	public List<Flight> getBy(String inputDest, LocalDate inputDate, int inputAmount){
		return FS.getBy(inputDest, inputDate, inputAmount);
	}
}
